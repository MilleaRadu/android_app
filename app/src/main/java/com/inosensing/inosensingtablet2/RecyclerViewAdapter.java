package com.inosensing.inosensingtablet2;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.StatView> {

    List<Integer> resources;
    List<String> labels;
    List<String> values;

    public RecyclerViewAdapter(List<Stat> stats) {

        resources = new ArrayList<Integer>();
        labels = new ArrayList<String>();
        values = new ArrayList<String>();

        stats.forEach(new Consumer<Stat>() {
            @Override
            public void accept(Stat stat) {
                resources.add(stat.getDrawableResource());
                labels.add(stat.getLabel());
                values.add(stat.getValue());
            }
        });
    }

    public class StatView extends RecyclerView.ViewHolder{

        ImageView image;
        TextView label;
        TextView value;

        public StatView(@NonNull View itemView) {

            super(itemView);
            image = itemView.findViewById(R.id.ImageRecyclerView);
            label = itemView.findViewById(R.id.TextViewLabelRecyclerView);
            value = itemView.findViewById(R.id.TextViewValueRectclerView);
        }
    }

    @NonNull
    @Override
    public StatView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stat,parent,false);
        return new StatView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StatView holder, int position) {
        holder.image.setImageResource(resources.get(position));
        holder.label.setText(labels.get(position));
        holder.value.setText(values.get(position));
    }



    @Override
    public int getItemCount() {
        return resources.size();
    }
}
