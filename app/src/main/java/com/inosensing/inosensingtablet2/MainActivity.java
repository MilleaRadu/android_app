package com.inosensing.inosensingtablet2;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.inosensing.inosensingtablet2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.WeakHashMap;

public class MainActivity extends AppCompatActivity  {

    private static final int REQUEST_INTERNET_PERMISSION = 9000;

    private BottomNavigationView navigation;
    private RecyclerView recyclerView;

    private TextView status;
    private TextView register;
    private TextView welcome;
    private Button logout;
    private Button login;

    private LinearLayout statsLayout;

    private boolean isLogged;
    private UUID user_id;
    List<Stat> stats ;

    TextView description;
    TextView temp;
    TextView feels_like;
    TextView temp_min;
    TextView temp_max ;
    TextView pressure;
    TextView humidity;
    TextView wind_speed;
    TextView wind_direction;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stats = new ArrayList<Stat>();

        //Log.d("TIMESTAMP", time);
        //yyy-MM-dd HH:mm:ss
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET},REQUEST_INTERNET_PERMISSION);
        }



        GridLayoutManager  gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);

        if(Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_USERNAME,ConstantsAndKeys.FILE_NAME_PREFS).equals("default")){
            welcome.setText("Welcome!");
            logout.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
            status.setText(R.string.notlog);
            status.setClickable(true);
            statsLayout.setVisibility(View.GONE);

            register.setText(R.string.registerHere);
            register.setClickable(true);
            isLogged=false;

        }else{
            user_id = UUID.fromString(Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_ID,ConstantsAndKeys.FILE_NAME_PREFS));
            FetchStats();
            welcome.setText("Hey, "+Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_USERNAME,ConstantsAndKeys.FILE_NAME_PREFS)+"!");
            status.setText(" Have a look at your statistics");
            statsLayout.setVisibility(View.VISIBLE);
            register.setVisibility(View.GONE);
            login.setVisibility(View.GONE);
            logout.setVisibility(View.VISIBLE);
            isLogged=true;
        }

        try {
            GetTheWeather();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });


        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.deletePrefs(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS);
                status.setText(R.string.notlog);
                status.setClickable(true);
                register.setText(R.string.registerHere);
                register.setClickable(true);
                register.setVisibility(View.VISIBLE);
                welcome.setText("Welcome!");
                login.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
                statsLayout.setVisibility(View.GONE);
            }
        });
        navigation = (BottomNavigationView)findViewById(R.id.bottomnav);

        navigation.setSelectedItemId(R.id.nav_home);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
               switch(item.getItemId()){
                   case R.id.nav_ride :
                       Intent ride = new Intent(getApplicationContext(), RideActivity.class);

                        startActivity(ride);
                       overridePendingTransition(1,1);
                       return true;
                   case R.id.nav_history:
                       Intent history = new Intent(getApplicationContext(), HistoryActivity.class);

                       startActivity(history);
                       overridePendingTransition(1,1);
                       return true;
                   case R.id.nav_help:
                       Intent help = new Intent(getApplicationContext(), HelpActivity.class);

                       startActivity(help);
                       overridePendingTransition(1,1);
                       return true;
                   case R.id.nav_home:
                       return true;

                   case R.id.    nav_overview:
                       Intent overviewIntent = new Intent(getApplicationContext(), OverviewActivity.class);
                       startActivity(overviewIntent);
               }
               return false;
            }
        });







    }


    private void InitViews(){
        recyclerView = findViewById(R.id.recyclerView);
        login = findViewById(R.id.login);
        logout = findViewById(R.id.logout);
        welcome = findViewById(R.id.WelcomeText);
        status = findViewById(R.id.textViewLoginStatus);
        register = findViewById(R.id.textViewGoToRegister);
        recyclerView = findViewById(R.id.recyclerView);
        statsLayout = findViewById(R.id.StatsLayout);
        description = findViewById(R.id.WeatherDescription);
        temp = findViewById(R.id.WeatherTemp);
        feels_like = findViewById(R.id.WeatherTempFeelsLike);
        temp_min = findViewById(R.id.WeatherTempMin);
        temp_max = findViewById(R.id.WeatherTempMax);
        pressure = findViewById(R.id.WeatherPressure);
        humidity = findViewById(R.id.WeatherHumidity);
        wind_speed = findViewById(R.id.WeatherWindSpeed);
//        wind_direction = findViewById(R.id.WeatherWindDir);
    }

    private void FetchStats() {

        AsyncTask<Void, Void, UserStats> weatherTask = new AsyncTask<Void, Void, UserStats>() {

            URL url;

            private void SetConnection(HttpURLConnection connection) throws ProtocolException {
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setUseCaches(false);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod("GET");
            }


            @Override
            protected UserStats doInBackground(Void... voids) {
                try {
                    url = new URL("http://inosensing.go.ro:8080/UserStats/"+user_id.toString());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader;
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    SetConnection(connection);
                    connection.connect();



                    int HttpResult = connection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        StringBuilder responseStrBuilder = new StringBuilder();
                        String inputStr;
                        while ((inputStr = reader.readLine()) != null)
                            responseStrBuilder.append(inputStr);
                        JSONObject response = new JSONObject(responseStrBuilder.toString());
                        double total_distance = response.getDouble("total_distance");
                        double max_avg_speed = response.getDouble("max_avg_speed");
                        int trails_no = response.getInt("trails_no");
                        int points_measured = response.getInt("points");
                        UserStats stats = new UserStats(total_distance,max_avg_speed,trails_no,points_measured);

                        return stats;
                    } else {

                        return null;
                    }


                } catch (
                        IOException e) {
                    e.printStackTrace();
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(UserStats s) {
                super.onPostExecute(s);
                if(s!=null){
                    PopulateStatsArea(s);
                }else{
                    Toast.makeText(MainActivity.this, "somth happend, weather is null.", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void PopulateStatsArea(UserStats s) {
        stats.add(new Stat(R.drawable.ic_bluetooth_searching_black_24dp,"Total points measured",String.valueOf(s.getPoints_measured())));
        stats.add(new Stat(R.drawable.ic_flash_on_black_24dp,"Maximum average speed",String.valueOf(Math.round(s.getMax_avg_speed()*3.6))+" km/h"));
        stats.add(new Stat(R.drawable.ic_list_numbered,"Number of trails",String.valueOf(s.getTrails_no())));
        stats.add(new Stat(R.drawable.ic_all_inclusive_black_24dp,"Total distance",String.valueOf(Math.floor(s.getTotal_distance()/1000.0*100)/100)+" km"));


        recyclerView.setAdapter(new RecyclerViewAdapter(stats));


    }

    private void GetTheWeather() throws IOException, JSONException {
        AsyncTask<Void, Void, Weather> weatherTask = new AsyncTask<Void, Void, Weather>() {

            URL url;
            private  void SetConnection(HttpURLConnection connection) throws ProtocolException {
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(10*1000);
                connection.setUseCaches(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod("GET");

            }

            @Override
            protected Weather doInBackground(Void... voids) {
                try {
                    url = new URL("http://api.openweathermap.org/data/2.5/weather?q=Bucharest&appid=6d87eb51ff917df274234919f91976e0&units=metric");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    BufferedReader reader;
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    connection.connect();


                    int HttpResult = connection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        StringBuilder responseStrBuilder = new StringBuilder();

                        String inputStr;
                        while ((inputStr = reader.readLine()) != null)
                            responseStrBuilder.append(inputStr);
                        JSONObject response = new JSONObject(responseStrBuilder.toString());
                        Weather weather = new Weather();
                        weather.setDescription(response.getJSONArray("weather").getJSONObject(0).getString("description"));
                        weather.setTemperature(response.getJSONObject("main").getDouble("temp"));
                        weather.setFeels_like(response.getJSONObject("main").getDouble("feels_like"));
                        weather.setTime_min(response.getJSONObject("main").getDouble("temp_min"));
                        weather.setTemp_max(response.getJSONObject("main").getDouble("temp_max"));
                        weather.setPressure(response.getJSONObject("main").getDouble("pressure"));
                        weather.setHumidity(response.getJSONObject("main").getDouble("humidity"));
                        weather.setWind_speed(response.getJSONObject("wind").getDouble("speed"));

                       // weather.setWind_direction(response.getJSONObject("wind").getDouble("deg"));
                        ////weather.setSunrise(new Date(Long.parseLong(String.valueOf(response.getJSONObject("sys").getDouble("sunrise")))).toString());
                       // weather.setSunrise(new Date(Long.parseLong(String.valueOf(response.getJSONObject("sys").getDouble("sunset")))).toString());

                        //SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                        return weather;
                    } else {

                        return null;
                    }


                } catch (
                        IOException e) {
                    e.printStackTrace();
                    return null;
                } catch (
                        JSONException e) {

                    e.printStackTrace();
                    return null;
                }


            }

            @Override
            protected void onPostExecute(Weather weather) {
                super.onPostExecute(weather);
                if(weather!=null){
                    PopulateWeatherArea(weather);
                }else{
                    Toast.makeText(MainActivity.this, "somth happend, weather is null.", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void PopulateWeatherArea(Weather weather) {



        description.setText(weather.getDescription().toUpperCase());
        temp.setText("now : "+String.valueOf(weather.getTemperature())+" °C");
        feels_like.setText("feels like : "+String.valueOf(weather.getFeels_like())+" °C");
        temp_min.setText("min : "+String.valueOf(weather.getTime_min())+" °C");
        temp_max.setText("max : "+String.valueOf(weather.getTemp_max())+" °C");
        pressure.setText("pressure : "+String.valueOf(weather.getPressure()*0.750061683)+" mmHg  ");
        humidity.setText("humidity : "+String.valueOf(weather.getHumidity())+ "100%");
        wind_speed.setText("wind speed : "+String.valueOf(weather.getWind_speed())+" ");
//        wind_direction.setText("wind direction : "+String.valueOf(weather.getWind_direction()));



    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "hey there", Toast.LENGTH_SHORT).show();
        navigation.setSelectedItemId(R.id.nav_home);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==REQUEST_INTERNET_PERMISSION&&grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
            return;
        }else{
            finish();
        }
    }
}
