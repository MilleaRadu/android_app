package com.inosensing.inosensingtablet2;

import android.content.Intent;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RegisterActivity extends AppCompatActivity {

    private final static String TAG = "RegisterActivity";

    private EditText surname;
    private EditText forname;
    private EditText email;
    private EditText username;
    private EditText password;
    private Button register ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        surname = findViewById(R.id.editTextSurname);
        forname = findViewById(R.id.editTextForname);
        email = findViewById(R.id.editTextEmail);
        username = findViewById(R.id.editTextUsername);
        password = findViewById(R.id.editTextPassword);
        register = findViewById(R.id.buttonRegister);
        RegisterListener();
    }

    public void RegisterListener(){
        register. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(surname.getText().toString().isEmpty()||forname.getText().toString().isEmpty()||email.getText().toString().isEmpty()||username.getText().toString().isEmpty()||password.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Please complete the registration form ", Toast.LENGTH_SHORT).show();
                }else{
                    //send registration to DB
                    RegisterUser();

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
        startActivity(intent);
    }

    public void RegisterUser(){

        AsyncTask<Void,Void,String> task = new AsyncTask<Void, Void, String>() {

            URL url;

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    url = new URL("http://inosensing.go.ro:8080/user");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    DataOutputStream printout;
                    DataInputStream input;
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    connection.setRequestProperty("Content-Type","application/json");
                    connection.setRequestMethod("POST");
                    connection.connect();
                    printout = new DataOutputStream(connection.getOutputStream());
                    printout.writeBytes(String.valueOf(ConstructBody()));
                    printout.flush();
                    printout.close();
                    int HttpResult = connection.getResponseCode();
                    if (HttpResult ==HttpURLConnection.HTTP_OK){
                        return "ACCEPTED";
                    }else{
                        return "ERROR POSTING";
                    }

                } catch (IOException | JSONException e) {
                    Log.d(TAG, "doInBackground: "+e.getLocalizedMessage());
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                if(s.equals("ACCEPTED")){
                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(RegisterActivity.this, "Registration failed", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
//
//        UID ID, String surname, String forname, String email, String password, String username
    }

    public JSONObject ConstructBody() throws JSONException {
        JSONObject body = new JSONObject();
        body.put("surname", surname.getText().toString().toLowerCase().trim());
        body.put("forname", forname.getText().toString().toLowerCase().trim());
        body.put("username", username.getText().toString().toLowerCase().trim());
        body.put("email", email.getText().toString().toLowerCase().trim());
        body.put("password", password.getText().toString().toLowerCase().trim());

        return body;
    }
}
