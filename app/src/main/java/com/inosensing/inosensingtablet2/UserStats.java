package com.inosensing.inosensingtablet2;

public class UserStats {

    private double total_distance;
    private double max_avg_speed;
    private int trails_no;
    private int points_measured;


    public UserStats(double total_distance, double max_avg_speed, int trails_no, int points_measured) {
        this.total_distance = total_distance;
        this.max_avg_speed = max_avg_speed;
        this.trails_no = trails_no;
        this.points_measured = points_measured;
    }

    public double getTotal_distance() {
        return total_distance;
    }

    public double getMax_avg_speed() {
        return max_avg_speed;
    }

    public int getTrails_no() {
        return trails_no;
    }

    public int getPoints_measured() {
        return points_measured;
    }
}
