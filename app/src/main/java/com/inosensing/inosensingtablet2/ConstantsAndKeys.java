package com.inosensing.inosensingtablet2;

public class ConstantsAndKeys {

    public static final String KEY_PREF_ID = "ID";
    public static final String KEY_PREF_MAC = "MAC";
    public static final String KEY_PREF_EMAIL = "EMAIL";
    public static final String KEY_PREF_FORNAME = "FORNAME";
    public static final String KEY_PREF_SURNAME = "SURNAME";
    public static final String KEY_PREF_PASSWORD = "PASSWORD";
    public static final String KEY_PREF_USERNAME = "USERNAME";
    public static final String FILE_NAME_PREFS = "INOSENSINGFILENAMEPREF";

}
