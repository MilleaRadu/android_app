package com.inosensing.inosensingtablet2;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.icu.util.Measure;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ExploreActivity extends AppCompatActivity {


    private static final int REQUEST_STORAGE_PERMISSIONS = 13;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private double distance;
    private double average_speed;
    private List<LatLng> points ;
    private String trail_id ;
    private String time_started;
    private String time_ended;
    private PolylineOptions polyline;
    private Polyline polylineP;
    private List<MeasurePoint> measurePoints = new ArrayList<MeasurePoint>();
    BarChart barChart;
    private Marker marker;
    private List<Marker> markerList = new ArrayList<Marker>();
    private static List<Float> hueColorCodes;
    private static DecimalFormat df2 = new DecimalFormat("#.##");


    TextView tvGreen;
    TextView tvOrange;
    TextView tvRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);
        hueColorCodes = new ArrayList<Float>();
        hueColorCodes.add(BitmapDescriptorFactory.HUE_GREEN);
        hueColorCodes.add(BitmapDescriptorFactory.HUE_ORANGE);
        hueColorCodes.add(BitmapDescriptorFactory.HUE_RED);

        tvGreen = findViewById(R.id.LegendGreen);

        tvOrange = findViewById(R.id.LegendOrange);

        tvRed = findViewById(R.id.LegendRed);

            Log.d("ExploreActivity", "permission is grented");
            Toast.makeText(this, "Permission is granted", Toast.LENGTH_LONG).show();
            Intent intent= getIntent();
            average_speed = intent.getDoubleExtra("average_speed",0);
            distance = intent.getDoubleExtra("distance",0);
            points = intent.getParcelableArrayListExtra("points");
            trail_id = intent.getStringExtra("trail_id");
            time_started = intent.getStringExtra("date_started");
            time_ended = intent.getStringExtra("date_ended");

            polyline= new PolylineOptions().addAll(points);
            polyline.color(Color.BLUE);
            polyline.width(30);

            barChart = findViewById(R.id.LineChartMeasurePoints);

            InitMap();
            Toast.makeText(this, String.valueOf(average_speed), Toast.LENGTH_SHORT).show();



    }

    private void FetchMeasurePoints() {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            URL url;

            @Override
            protected String doInBackground(Void... voids) {

                try {
                    url = new URL("http://inosensing.go.ro:8080/geoPoint/" + trail_id);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    BufferedReader reader;

                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    connection.setUseCaches(false);
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestMethod("GET");
                    connection.connect();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                    int HttpResult = connection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        StringBuilder responseStrBuilder = new StringBuilder();

                        String inputStr;
                        while ((inputStr = reader.readLine()) != null)
                            responseStrBuilder.append(inputStr);
                        JSONObject response = new JSONObject(responseStrBuilder.toString());
                        //should do all hard work here and return thee list .. so change tu return List<Trip>
                        JSONArray objects = response.getJSONArray("rows");
                        for (int i = 0; i < objects.length(); i++) {
                            JSONObject object = objects.getJSONObject(i);
                            String datetime = object.getString("datetime");
                            String id = object.getString("id");
                            double temperature = object.getDouble("temperature");
                            double humidity = object.getDouble("humidity");
                            double dust = object.getDouble("dust");
                            String geom = object.getString("geom");
                            String[] coords = geom.split(" ");
                            double lng = Double.valueOf(coords[0].substring(6, coords[0].length() ));
                            double lat = Double.valueOf(coords[1].substring(0, coords[1].length()-1 ));


                            measurePoints.add(new MeasurePoint(dust, humidity, temperature, new LatLng(lat, lng),datetime));


                        }
                        //SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                        return "ACCEPTED";
                    } else {

                        return "ERROR";
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                if(s.equals("ACCEPTED")){
                    if(measurePoints.size()>2){
                        List<Double> dustValues = new ArrayList<Double>();
                        for(int i = 0 ; i < measurePoints.size();i++){
                            dustValues.add(measurePoints.get(i).getDust());
                        }


                        List<Double> outliers = getOutliers(dustValues);
                        Log.d("ExploreActivity", "onPostExecute: outliers :  " + outliers.toString());
                        List<MeasurePoint> clone = new ArrayList<>();
                        clone.addAll(measurePoints);

                        clone.removeIf(n->(outliers.contains(n.getDust())));
                        clone.sort(new Comparator<MeasurePoint>() {
                            @Override
                            public int compare(MeasurePoint o1, MeasurePoint o2) {
                                if(o1.getDust()<o2.getDust()){
                                    return -1;
                                }else{
                                    return 1;
                                }
                            }
                        });
                        PopulateLegend(clone);
                        Log.d("ExploreActivity", "onPostExecute: clone : " + clone.toString());
                        AddColoredMarker(clone.get(0).getDust(),clone.get(clone.size()-1).getDust());
                    }else{
                        PopulateLegend(measurePoints);
                        AddColoredMarker(MeasurePoint.minValue,MeasurePoint.maxValue);
                    }

                    try {
                        PopulateChart();
                    } catch (ParseException e) {
                        Log.d("ExploreActivity", e.toString());
                    }
                }else{
                    Toast.makeText(ExploreActivity.this, "SOMTHING WENT WRONG", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }

    private void PopulateLegend(List<MeasurePoint> clone) {

        if(clone.size()==0){
            return;
        }

        clone.sort(new Comparator<MeasurePoint>() {
            @Override
            public int compare(MeasurePoint o1, MeasurePoint o2) {
                if(o1.getDust()<o2.getDust()){
                    return -1;
                }else{
                    return 1;
                }
            }
        });

        double first_limit;
        double second_limit;

        first_limit = (clone.get(clone.size()-1).getDust()-clone.get(0).getDust())/3.0;
        second_limit = (clone.get(clone.size()-1).getDust()-clone.get(0).getDust())/3.0+((clone.get(clone.size()-1).getDust()-clone.get(0).getDust())/3.0);

        tvGreen.setText("0 < X < "+(df2.format(clone.get(0).getDust()+first_limit)));
        tvOrange.setText(df2.format(clone.get(0).getDust()+first_limit)+" <= X <= "+(df2.format(clone.get(0).getDust()+second_limit)));
        tvRed.setText( (df2.format(clone.get(0).getDust()+second_limit))+" < X ");

    }

    private void AddColoredMarker(double in_min,double in_max) {

        for( int i = 0 ; i < measurePoints.size();i++){
//                        return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
            int colorSeverity = (int)(Math.round((measurePoints.get(i).getDust()-in_min)*(3-1)/((in_max - in_min))+1));
            Log.d("ExploreActivity", "onPostExecute: item dust "+ measurePoints.get(i).getDust());
            Log.d("ExploreActivity", "onPostExecute: "+ colorSeverity);
            if(colorSeverity>3){
                colorSeverity=3;
            }
            if(colorSeverity<1){
                colorSeverity=1;
            }
            marker = map.addMarker(new MarkerOptions().position(measurePoints.get(i).getCoords())
                    .icon(BitmapDescriptorFactory.defaultMarker(hueColorCodes.get(colorSeverity-1)))//EnumColorMap.values()[colorSeverity]
                    .title("PM Concentration : ")
                    .snippet(String.valueOf(measurePoints.get(i).getDust())));
            markerList.add(marker);
        }
    }


    public void InitMap () {
                    mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.MapFragmentExplore);
                    assert mapFragment != null;
                    mapFragment.getMapAsync(googleMap -> {

                        //  googleMap.addPolyline(polyline);
//                    map.setTrafficEnabled(true);
//                    map.setBuildingsEnabled(true);

                        map = googleMap;
                        polylineP = map.addPolyline(polyline);
                        FetchMeasurePoints();




                        //map.addMarker(new MarkerOptions().position(new LatLng(44.4911213,26.0386888)).title("HEY THEREEE"));
                    });
                }

    private void PopulateChart() throws ParseException {
        ArrayList time = new ArrayList();
        ArrayList concentrations = new ArrayList();
        if(measurePoints.size()==0){
            return;
        }
       for(int i = 0 ; i< measurePoints.size();i++){
           time.add(new BarEntry(i,i));
           concentrations.add(new BarEntry(i,(float)measurePoints.get(i).getDust()));
       }
        Log.d("ExploreActivity", "time :  "+time.size() + "concentrations  : " + concentrations.size());

       BarDataSet bardataset = new BarDataSet(concentrations,"Concentrations along the way in chronological order, measured in ug/m3");

       BarDataSet yAxis = new BarDataSet(time,"Time");
       BarData data = new BarData( bardataset);
       bardataset.setColors(Color.rgb(109,176,252));


       barChart.setData(data);
       barChart.invalidate();
       map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(measurePoints.get(0).getCoords(),12f,90f,0)));
       barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
           @Override
           public void onValueSelected(Entry e, Highlight h) {
               marker.hideInfoWindow();

               Log.d("ExploreActivity", "onValueSelected: ");

               map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(measurePoints.get(bardataset.getEntryIndex(e)).getCoords(),18f,90f,0)));
               marker = markerList.get(bardataset.getEntryIndex(e));
               markerList.get(bardataset.getEntryIndex(e)).showInfoWindow();
                //play with info windows

           }

           @Override
           public void onNothingSelected() {

               map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(measurePoints.get(0).getCoords(),12f,90f,0)));
               marker.hideInfoWindow();

           }
       });
    }

    @Override
                public void onBackPressed () {
                    Intent intent = new Intent(ExploreActivity.this, HistoryActivity.class);
                    startActivity(intent);
                    finish();
                }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "ExploreActivity destroyed", Toast.LENGTH_SHORT).show();
        Log.d("ExploreActivity", "onDestroy: "+MeasurePoint.maxValue+" "+ MeasurePoint.minValue+ " max - min before");
        MeasurePoint.ResetInterval();
        Log.d("ExploreActivity", "onDestroy: "+MeasurePoint.maxValue+" "+ MeasurePoint.minValue+ " max - min after");
    }

    public static List<Double> getOutliers(List<Double> input) {
        Collections.sort(input);
        List<Double> output = new ArrayList<Double>();
        List<Double> data1 = new ArrayList<Double>();
        List<Double> data2 = new ArrayList<Double>();
        if (input.size() % 2 == 0) {
            data1 = input.subList(0, input.size() / 2);
            data2 = input.subList(input.size() / 2, input.size());
        } else {
            data1 = input.subList(0, input.size() / 2);
            data2 = input.subList(input.size() / 2 + 1, input.size());
        }
        double q1 = getMedian(data1);
        double q3 = getMedian(data2);
        double iqr = q3 - q1;
        double lowerFence = q1 - 1.5 * iqr;
        double upperFence = q3 + 1.5 * iqr;

        for (int i = 0; i < input.size(); i++) {
            if (input.get(i) < lowerFence || input.get(i) > upperFence)
                output.add(input.get(i));
        }
        return output;
    }

    private static double getMedian(List<Double> data) {

        if (data.size() % 2 == 0)
            return (data.get(data.size() / 2) + data.get(data.size() / 2 - 1)) / 2;
        else
            return data.get(data.size() / 2);
    }
}

