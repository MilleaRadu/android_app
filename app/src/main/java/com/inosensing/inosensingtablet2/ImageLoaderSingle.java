package com.inosensing.inosensingtablet2;

import android.content.Context;

import com.example.alvis.lazyload.ImageLoader;

public class ImageLoaderSingle {

    public static ImageLoader imageLoader;

    public ImageLoaderSingle(Context context){
        imageLoader = new ImageLoader(context);
    }

    public ImageLoader getImageLoader(){
        return imageLoader;
    }
}
