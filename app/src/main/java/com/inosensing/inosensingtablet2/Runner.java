package com.inosensing.inosensingtablet2;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Runner implements Runnable {

    public static final String DEVICE_DISCONNECTED = "com.inosensing.inosensingtablet2.DEVICE_DISCONNECTED";
    public static String HM_10_CHARACTERISTIC_UUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    public static String Hm_10_SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";

    public int mConnectionState = STATE_DISCONNECTED;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String NEW_LOCATION_AND_SPEED = "com.inosensing.inosensingtablet2.NEW_LOCATION_AND_SPEED";
    public final static String ACTION_GATT_CONNECTED =
            "com.inosensing.inosensingtablet2.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.inosensing.inosensingtablet2.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.inosensing.inosensingtablet2.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.inosensing.inosensingtablet2.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.inosensing.inosensingtablet2.EXTRA_DATA";

    public final static String DISPLAY_DATA=
            "com.inosensing.inosensingtablet2.DISPLAY_DATA";
    public final static String DATA_FROM_INO= "com.inosensing.inosensingtablet2.DATA_FROM_INO";
    public final static String DATA_SPEED= "com.inosensing.inosensingtablet2.DATA_SPEED";
    public final static String DATA_LATITUDE="com.inosensing.inosensingtablet2.DATA_LATITUDE";
    public final static String DATA_LONGITUDE="com.inosensing.inosensingtablet2.DATA_LONGITUDE";

    private int period;
    public boolean servicesOk=false;
    private String dataRead;
    private String deviceAdress;
    public static final String TAG = ForegroundService.class.getSimpleName();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    protected BluetoothGatt mBluetoothGatt;
    private BluetoothGattCharacteristic characteristic;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Context context;
    private float humidity;
    private float temperature;
    private float dust;
    private double latitude;
    private double longitude;
    private double speed=0;
    private UUID trail_id;
    private UUID user_id;
    private boolean shouldBeKilled  = false;

    public static List<LatLng> mapPoints;
    public static List<Double> speedList;
    //private ExecutorService executorService;//dau submit la fiecare metoda din "RUN";

    public Runner(int period, String deviceAdress, Context context, String trail_id){
        this.period=period;
        this.deviceAdress=deviceAdress;
        this.context = context;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        locationRequest=new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(7000);
        this.trail_id = UUID.fromString(trail_id);
        this.user_id = UUID.fromString(Preferences.getSharedPreferences(context,ConstantsAndKeys.KEY_PREF_ID,ConstantsAndKeys.FILE_NAME_PREFS));
        mapPoints = new ArrayList<LatLng>();
        speedList = new ArrayList<Double>();
        LocationCallback();
        mFusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
        boolean good = initialize();
        if(good){
            connect(deviceAdress);


        }else{
            
        }
    }

    public void InitGATT(BluetoothGatt gatt){

        mBluetoothGatt = gatt;
        mBluetoothGatt.discoverServices();



    }

    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.d(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }


        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        mBluetoothGatt = device.connectGatt(context, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }


    public boolean initialize() {

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothAdapter=null;
        mBluetoothGatt.disconnect();
        mBluetoothGatt.close();
        mBluetoothGatt=null;
        mConnectionState = STATE_DISCONNECTED;
    }


    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
        mBluetoothDeviceAddress=null;
        mBluetoothAdapter.disable();
        mBluetoothManager=null;
        mBluetoothGatt.close();
        mBluetoothGatt=null;
        mBluetoothGatt = null;
    }


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                InitGATT(gatt);
                mConnectionState = STATE_CONNECTED;

                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");

                Log.i(TAG, "Attempting to start service discovery:" );


            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                gatt.close();
                context.sendBroadcast(new Intent(DEVICE_DISCONNECTED));
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }


        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onCharacteristicWrite: GATT_SUCCESS ,sending broadcast data available ");


            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG,"onServicesDiscovered");
            super.onServicesDiscovered(gatt, status);
            Log.d("OnservicesDiscovered","movin on");
            characteristic = mBluetoothGatt.getService(UUID.fromString(Hm_10_SERVICE_UUID)).getCharacteristic(UUID.fromString(HM_10_CHARACTERISTIC_UUID));
            if(characteristic!=null){
                servicesOk=true;
            }else{
                Log.d("OnservicesDiscovered","characteristic is null");
                shouldBeKilled=true;

            }
            Log.d(TAG,"characteristic  "+characteristic);
            final int charaProp= characteristic.getProperties();
            Log.d(TAG,"charaprop  "+charaProp);
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {

                if (mNotifyCharacteristic != null) {
                    setCharacteristicNotification(
                            mNotifyCharacteristic, false);
                    mNotifyCharacteristic = null;
                }

            }
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {

                if (mNotifyCharacteristic != null) {
                    setCharacteristicNotification(
                            mNotifyCharacteristic, false);
                    mNotifyCharacteristic = null;
                }



            }if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                mNotifyCharacteristic = characteristic;
                setCharacteristicNotification(
                        characteristic, true);
            }



        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {

            Log.d(TAG, "onCharacteristicChanged :  ive been called ");
            broadcastUpdate(DISPLAY_DATA, characteristic);

        }


    };

    private void broadcastUpdate(String action) {
        final Intent intent = new Intent(action);
        Log.d(TAG, "broadcastUpdate: sent simple broadcast");
        context.sendBroadcast(intent);
    }

    private void broadcastUpdate(String action,BluetoothGattCharacteristic characteristic) {
        //final Intent intent = new Intent(action);


        Log.v("AndroidLE", "broadcastUpdate()");

        final byte[] data = characteristic.getValue();

        Log.v("AndroidLE", "data.length" + data.length);

        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for (byte byteChar : data) {
                stringBuilder.append(String.format("%02X ", byteChar));
                Log.v("AndroidLE", String.format("%02X ", byteChar));
            }
            dataRead = new String(data);
            SendAllInfoToActivity();
            //intent.putExtra(EXTRA_DATA, dataRead);
        }
        Log.d(TAG, "broadcastUpdate: sending big broadcast");
        //context.sendBroadcast(intent);
        if(latitude!=0&&longitude!=0){
            AsyncTask task = new AsyncTask<JSONObject,Void,String>(){
                URL url;
                @Override
                protected String doInBackground(JSONObject... jsonObjects) {
                    try {
                        url = new URL("http://inosensing.go.ro:8080/geoPoint");
                        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                        DataOutputStream printout;
                        DataInputStream input;
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        connection.setUseCaches(false);
                        connection.setRequestProperty("Content-Type","application/json");
                        connection.setRequestMethod("POST");
                        connection.connect();
                        printout = new DataOutputStream(connection.getOutputStream());
                        printout.writeBytes(String.valueOf(jsonObjects[0]));
                        printout.flush();
                        printout.close();
                        int HttpResult = connection.getResponseCode();
                        if (HttpResult ==HttpURLConnection.HTTP_OK){
                            return "ACCEPTED";
                        }else{
                            return "ERROR POSTING";
                        }

                    } catch (IOException e) {
                        Log.d(TAG, "doInBackground: "+e.getLocalizedMessage());
                        return null;
                    }

                }

                @Override
                protected void onPostExecute(String s) {
                    Log.d("ForegroundService","POST MEASURE"+ s);
                }
            }.execute(JSONFormatter());

        }



    }

    private JSONObject JSONFormatter() {
        try {
            final JSONObject obj = new JSONObject();
//            textMeasureDust.setText(dataSplit[2]+"ug/m3 PM");
//            textMeasureTemp.setText(dataSplit[1]+"Celsius Temp");
//            textMeasureHum.setText(dataSplit[0]+"% Relative Humidity");
            String[] dataArray=dataRead.split(" ");
            obj.put("dust",dataArray[1]);
            obj.put("temperature",dataArray[0]);
            obj.put("humidity", dataArray[2]);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);
            obj.put("trail_id",trail_id.toString());
            obj.put("user_id",user_id.toString());


            return obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    private void LocationCallback(){
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                        latitude = locationResult.getLastLocation().getLatitude();
                        longitude = locationResult.getLastLocation().getLongitude();
                        mapPoints.add(new LatLng(latitude,longitude));
                        if(locationResult.getLastLocation().hasSpeed()){
                            speed = locationResult.getLastLocation().getSpeed();
                            speedList.add(speed);
                            Log.d(TAG,"HAS SPEED");



                        }else{
                            Log.d(TAG,"dose not have speed");
                        }
                        Log.d(TAG,locationResult.getLastLocation().toString());

                        SendTripAckToActivity();

                        Log.d(TAG, "onLocationResult:"+latitude+" "+longitude);
                        //JSONObject obj = JsonFormatter(location);
                       // Toast.makeText(context, obj.toString(), Toast.LENGTH_SHORT).show();

                        //SendGEOJSON(obj);
                        //ar trebui aici JSON FORMATTER(LOCATION)
//                        if(mFusedLocationProviderClient!=null){
//                            mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
//                        }

//                        LocationsView.setText(String.format(Locale.US,"%s -- %s", location.getLatitude(),location.getLongitude()));

                    Log.d(TAG, "onLocationResult: LOCATION IN FOR");



            }
        };
    }


    public Runner() {
        super();
    }

    @Override
    public void run() {
        while(!servicesOk&&!shouldBeKilled){
                Log.d("ForegroundService","waiting for ok");
        }

        if(shouldBeKilled){
            return;
        }else{
            while(ForegroundService.running&&mConnectionState==STATE_CONNECTED){
                Log.d(TAG, "run: ENTERED");
                RequestMeasurements();
                //GetLastLocation();
                //SendAllInfoToActivity();
                //SendDataToServer();
                try {
                    Wait();
                } catch (InterruptedException e) {
                    Log.d(TAG, "runner has been interrupted in wait");
                    break;
                }

            }
        }
//        Log.d(TAG,"DONE WITH SERVICES OK ");
//        if(ForegroundService.running==false){
//            Log.d(TAG, "run: running is false");
//        }

        context.sendBroadcast(new Intent(DEVICE_DISCONNECTED));
    }

    private void SendTripAckToActivity(){
        Intent intent = new Intent(NEW_LOCATION_AND_SPEED);
        Log.d(TAG,"send+tripinfo_activity");
//        intent.putExtra(DATA_SPEED,speed);
        context.sendBroadcast(intent);

    }

    private void SendAllInfoToActivity(){
        Intent intent = new Intent(DISPLAY_DATA);
        Log.d(TAG+' '+"SEND_TO_ACTIVITY",dataRead + " ");
        intent.putExtra(DATA_FROM_INO,dataRead);
        intent.putExtra(DATA_LATITUDE,latitude);
        intent.putExtra(DATA_LONGITUDE,longitude);
        context.sendBroadcast(intent);
    }

    private void Wait() throws InterruptedException {
        TimeUnit.SECONDS.sleep(30);
    }



    private void GetLastLocation() {

        if(ActivityCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            return;
        }

        mFusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful()) {
                    Location location = task.getResult();
                    //doar interoghezi providderul aici
                    if (location != null) {
                        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                        //BLEInput.setText(DATA+" "+location.getLongitude()+location.getLatitude());
                        //SendGEOJSON(JsonFormatter(location));


                    } else {
                        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                        Log.d(TAG, "onComplete: LOCATION IS NULL");
                    }


                }

            }  }); }



    private void RequestMeasurements() {
        Log.d(TAG,"SENDING INPUT IN REQUEST MEASUREMENTS");
        byte[] output = "a".getBytes();
        if(characteristic!=null){
            send(output,characteristic);
        }else{
            characteristic = mBluetoothGatt.getService(UUID.fromString(Hm_10_SERVICE_UUID)).getCharacteristic(UUID.fromString(HM_10_CHARACTERISTIC_UUID));
            send(output,characteristic);

        }

    }


    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }


    public boolean send(byte[] data, BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "cannot write( method send)");
            Toast.makeText(context, "S_A PIERDUT CONN", Toast.LENGTH_LONG);
            return false;

//        BluetoothGattCharacteristic characteristic =
//                mGattCharacteristics.get(3).getCharacteristic(UUID_SEND);


        }
        characteristic.setValue(data);
        characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);

        mBluetoothGatt.writeCharacteristic(characteristic);
        return true;
    }


    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.d(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
    }


}
