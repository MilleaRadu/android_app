package com.inosensing.inosensingtablet2;

public class Stat {

    private int drawableResource;
    private String Label;
    private String Value;

    public Stat(int drawableResource, String label, String value) {
        this.drawableResource = drawableResource;
        Label = label;
        Value = value;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
