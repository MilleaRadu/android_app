package com.inosensing.inosensingtablet2;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.inosensing.inosensingtablet2.R;

public class HelpActivity extends AppCompatActivity {

    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        navigation = (BottomNavigationView)findViewById(R.id.bottomnav);

        navigation.setSelectedItemId(R.id.nav_help);



        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nav_ride :
                        Intent ride = new Intent(getApplicationContext(), RideActivity.class);
                        startActivity(ride);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_overview :
                        Intent overview = new Intent(getApplicationContext(), OverviewActivity.class);
                        startActivity(overview);
                        overridePendingTransition(1,1);
                        return true;


                    case R.id.nav_history:
                        Intent history = new Intent(getApplicationContext(), HistoryActivity.class);
                        startActivity(history);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_help:

                        return true;
                    case R.id.nav_home :
                        Intent home = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(home);
                        overridePendingTransition(1,1);
                        return true;

                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(R.id.nav_help);
    }
}
