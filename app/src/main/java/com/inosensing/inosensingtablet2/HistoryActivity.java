package com.inosensing.inosensingtablet2;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



public class HistoryActivity extends AppCompatActivity {

    private static final int REQUEST_STORAGE_PERMISSIONS = 13;
    private BottomNavigationView navigation;
    private Button previousButton;
    private Button nextButton;
    List<Trip> tripsList = new ArrayList<Trip>();
    ListAdapterTrail adapterTrail ;
    String user_id;
    private ListView listViewTrails;
    private int trailNo;
    private int offset;
    private int limit=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        previousButton = findViewById(R.id.ButtonPrevious);
        nextButton = findViewById(R.id.ButtonNext);

        PreviousButtonListener();
        NextButtonListener();

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission is not granted", Toast.LENGTH_LONG).show();
            Log.d("ExploreActivity", "permission is not grented");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS);
        }else{
            user_id = Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_ID,ConstantsAndKeys.FILE_NAME_PREFS);

            navigation = (BottomNavigationView)findViewById(R.id.bottomnav);
            listViewTrails = findViewById(R.id.ListViewTrails);
            ListViewListener();

            navigation.setSelectedItemId(R.id.nav_history);

            if("default".equals(user_id.toLowerCase().trim())){
                android.app.AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
                builder.setMessage("Please log in or register");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
            }else{
                offset = 0;
                GetTrailPage(user_id,limit,offset);
            }




            navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.nav_overview:
                            Intent overview = new Intent(getApplicationContext(),OverviewActivity.class);
                            startActivity(overview);
                            overridePendingTransition(1,1);
                            return true;
                        case R.id.nav_ride :
                            Intent ride = new Intent(getApplicationContext(), RideActivity.class);

                            startActivity(ride);
                            overridePendingTransition(1,1);
                            return true;
                        case R.id.nav_history:
                            return true;

                        case R.id.nav_help:
                            Intent help = new Intent(getApplicationContext(), HelpActivity.class);

                            startActivity(help);
                            overridePendingTransition(1,1);
                            return true;

                        case R.id.nav_home :
                            Intent home = new Intent(getApplicationContext(), MainActivity.class);

                            startActivity(home);
                            overridePendingTransition(1,1);
                            return true;
                    }
                    return false;
                }
            });
        }

        GetTrailCount();

    }

    private void NextButtonListener() {

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(limit*(offset+1)>=trailNo){

                    Toast.makeText(HistoryActivity.this, "These are the last trails", Toast.LENGTH_SHORT).show();

                }else{
                    offset+=1;
                    GetTrailPage(user_id,limit,offset);
                }
            }
        });



    }

    private void PreviousButtonListener() {

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(offset-1==-1){
                    Toast.makeText(HistoryActivity.this, "This is the first page", Toast.LENGTH_SHORT).show();
                }else{
                    offset-=1;
                    GetTrailPage(user_id,limit,offset);
                }
            }
        });

    }

    private void GetTrailPage(String user_id, int limit, int offset) {
        tripsList = new ArrayList<Trip>();
        InitAdapter();
        AsyncTask<String,Void,String> task = new AsyncTask<String, Void, String>() {

            URL url;
            @Override
            protected String doInBackground(String... strings) {
                try {
                    url = new URL("http://inosensing.go.ro:8080/trail/"+strings[0]+"/"+limit+"/"+offset);

                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    BufferedReader reader;

                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    connection.setUseCaches(false);
                    connection.setRequestProperty("Content-Type","application/json");
                    connection.setRequestMethod("GET");
                    connection.connect();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                    int HttpResult = connection.getResponseCode();
                    if (HttpResult ==HttpURLConnection.HTTP_OK){
                        StringBuilder responseStrBuilder = new StringBuilder();

                        String inputStr;
                        while ((inputStr = reader.readLine()) != null)
                            responseStrBuilder.append(inputStr);
                        JSONObject response = new JSONObject(responseStrBuilder.toString());
                        //should do all hard work here and return thee list .. so change tu return List<Trip>
                        JSONArray objects =response.getJSONArray("rows");
                        //SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                        for(int i=0;i<objects.length();i++){
                            JSONObject object = objects.getJSONObject(i);
                            String trail_id = object.getString("trail_id");
                            String user_id = object.getString("user_id");
                            double average_speed = Double.valueOf(object.getString("average_speed"));
                            String time_started = object.getString("time_started");
                            String time_ended = object.getString("time_ended");
                            double distance = Double.valueOf(object.getString("distance"));
                            String geom = object.getString("geom");
                            String[] linestring = geom.split(",");
                            linestring[0]=linestring[0].substring(11,linestring[0].length()-1);
                            linestring[linestring.length-1]=linestring[linestring.length-1].substring(0,linestring[linestring.length-1].length()-2);
                            Log.d("HistoryActivity", linestring[0]+" linestring de 0");
                            Log.d("HistoryActivity", linestring[linestring.length-1]+" linestring de ultimul");
                            List<LatLng> polyline = new ArrayList<LatLng>();
                            for(int j=0;j<linestring.length;j++){
                                LatLng latLng = new LatLng(Double.valueOf(linestring[j].split(" ")[1]),Double.valueOf(linestring[j].split(" ")[0]));
                                polyline.add(latLng);
                            }
                            Log.d("HistoryActivity", polyline.get(0).toString());
                            tripsList.add(new Trip(polyline,average_speed,distance,time_started,time_ended,trail_id));
                        }

                        return "tripList should be populated";

                    }else{
                        return "ERROR POSTING";
                    }
                } catch (MalformedURLException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(HistoryActivity.this, s, Toast.LENGTH_SHORT).show();
                if("ERROR POSTING".equals(s)){
                    Toast.makeText(HistoryActivity.this, "adapter not initialized", Toast.LENGTH_SHORT).show();
                }else{
                    adapterTrail.notifyDataSetChanged();
                }
            }
        }.execute(user_id);
    }

    private void GetTrailCount() {

        AsyncTask<String,Void,String> task = new AsyncTask<String, Void, String>() {

            URL url;
            @Override
            protected String doInBackground(String... strings) {
                try {
                    url = new URL("http://inosensing.go.ro:8080/trailNo/"+strings[0]);

                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    BufferedReader reader;

                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    connection.setUseCaches(false);
                    connection.setRequestProperty("Content-Type","application/json");
                    connection.setRequestMethod("GET");
                    connection.connect();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                    int HttpResult = connection.getResponseCode();
                    if (HttpResult ==HttpURLConnection.HTTP_OK){
                        StringBuilder responseStrBuilder = new StringBuilder();

                        String inputStr;
                        while ((inputStr = reader.readLine()) != null)
                            responseStrBuilder.append(inputStr);
                        JSONObject response = new JSONObject(responseStrBuilder.toString());
                        //should do all hard work here and return thee list .. so change tu return List<Trip>
                        JSONArray objects =response.getJSONArray("rows");
                        //SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                        trailNo=objects.getJSONObject(0).getInt("count");
                        return "tripNo should be ok";

                    }else{
                        return "ERROR Getting Trail No";
                    }
                } catch (MalformedURLException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.d("HistoryActivity", e.getLocalizedMessage());
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(HistoryActivity.this, s, Toast.LENGTH_SHORT).show();
                if("ERROR POSTING".equals(s)){
                    Toast.makeText(HistoryActivity.this, "error when getting trail no ", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(HistoryActivity.this, "got trail no ", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute(user_id);

    }

    public void InitAdapter(){
        adapterTrail = new ListAdapterTrail(this,R.layout.adapter_view_trails,tripsList);

        listViewTrails.setAdapter(adapterTrail);

    }

    private void ListViewListener() {
        listViewTrails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trip exploredTrip =tripsList.get(position);
                Intent intent = new Intent(HistoryActivity.this,ExploreActivity.class);
                intent.putExtra("average_speed",exploredTrip.getAvgSpeed());
                intent.putExtra("distance",exploredTrip.getDistance());
                intent.putExtra("trail_id",exploredTrip.getTrail_id());
                intent.putExtra("date_started",exploredTrip.getDate_started());
                intent.putExtra("date_ended",exploredTrip.getDate_ended());
                intent.putParcelableArrayListExtra("points", (ArrayList<? extends Parcelable>) exploredTrip.getPolylineBuffer());

                startActivity(intent);
                finish();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(R.id.nav_history);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch(requestCode){
            case REQUEST_STORAGE_PERMISSIONS:{
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Continue();
                }else{
                    finish();
                }

            }

        }

    }

    private void Continue() {
        user_id = Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_ID,ConstantsAndKeys.FILE_NAME_PREFS);

        navigation = (BottomNavigationView)findViewById(R.id.bottomnav);

        navigation.setSelectedItemId(R.id.nav_history);

        if("default".equals(user_id.toLowerCase().trim())){
            android.app.AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
            builder.setMessage("Please log in or register");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        }else{
            AsyncTask<String,Void,String> task = new AsyncTask<String, Void, String>() {

                URL url;
                @Override
                protected String doInBackground(String... strings) {
                    try {
                        url = new URL("http://inosensing.go.ro:8080/trail/"+strings[0]);

                        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                        BufferedReader reader;

                        connection.setDoInput(true);
                        connection.setDoOutput(false);
                        connection.setUseCaches(false);
                        connection.setRequestProperty("Content-Type","application/json");
                        connection.setRequestMethod("GET");
                        connection.connect();
                        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                        int HttpResult = connection.getResponseCode();
                        if (HttpResult ==HttpURLConnection.HTTP_OK){
                            StringBuilder responseStrBuilder = new StringBuilder();

                            String inputStr;
                            while ((inputStr = reader.readLine()) != null)
                                responseStrBuilder.append(inputStr);
                            JSONObject response = new JSONObject(responseStrBuilder.toString());
                            //should do all hard work here and return thee list .. so change tu return List<Trip>
                            JSONArray objects =response.getJSONArray("rows");
                            //SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                            for(int i=0;i<objects.length();i++){
                                JSONObject object = objects.getJSONObject(i);
                                String trail_id = object.getString("trail_id");
                                String user_id = object.getString("user_id");
                                double average_speed = Double.valueOf(object.getString("average_speed"));
                                String time_started = object.getString("time_started");
                                String time_ended = object.getString("time_ended");
                                double distance = Double.valueOf(object.getString("distance"));
                                String geom = object.getString("geom");
                                String[] linestring = geom.split(",");
                                linestring[0]=linestring[0].substring(11,linestring[0].length()-1);
                                linestring[linestring.length-1]=linestring[linestring.length-1].substring(0,linestring[linestring.length-1].length()-2);
                                Log.d("HistoryActivity", linestring[0]+" linestring de 0");
                                Log.d("HistoryActivity", linestring[linestring.length-1]+" linestring de ultimul");
                                List<LatLng> polyline = new ArrayList<LatLng>();
                                for(int j=0;j<linestring.length;j++){
                                    LatLng latLng = new LatLng(Double.valueOf(linestring[j].split(" ")[1]),Double.valueOf(linestring[j].split(" ")[0]));
                                    polyline.add(latLng);
                                }
                                Log.d("HistoryActivity", polyline.get(0).toString());
                                tripsList.add(new Trip(polyline,average_speed,distance,time_started,time_ended,trail_id));
                            }
                            return "tripList should be populated";

                        }else{
                            return "ERROR POSTING";
                        }
                    } catch (MalformedURLException e) {
                        Log.d("HistoryActivity", e.getLocalizedMessage());
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        Log.d("HistoryActivity", e.getLocalizedMessage());
                        e.printStackTrace();
                    } catch (IOException e) {
                        Log.d("HistoryActivity", e.getLocalizedMessage());
                        e.printStackTrace();
                    } catch (JSONException e) {
                        Log.d("HistoryActivity", e.getLocalizedMessage());
                        e.printStackTrace();
                    }


                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    Toast.makeText(HistoryActivity.this, s, Toast.LENGTH_SHORT).show();
                    if("ERROR POSTING".equals(s)){
                        Toast.makeText(HistoryActivity.this, "adapter not initialized", Toast.LENGTH_SHORT).show();
                    }else{
                        InitAdapter();
                    }
                }
            }.execute(user_id);
        }




        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nav_overview:
                        Intent overview = new Intent(getApplicationContext(),OverviewActivity.class);
                        startActivity(overview);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_ride :
                        Intent ride = new Intent(getApplicationContext(), RideActivity.class);

                        startActivity(ride);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_history:
                        return true;

                    case R.id.nav_help:
                        Intent help = new Intent(getApplicationContext(), HelpActivity.class);

                        startActivity(help);
                        overridePendingTransition(1,1);
                        return true;

                    case R.id.nav_home :
                        Intent home = new Intent(getApplicationContext(), MainActivity.class);

                        startActivity(home);
                        overridePendingTransition(1,1);
                        return true;
                }
                return false;
            }
        });


    }
}
