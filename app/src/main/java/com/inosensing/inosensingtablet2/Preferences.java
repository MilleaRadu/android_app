package com.inosensing.inosensingtablet2;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {



    public static String getSharedPreferences(Context context, String key,String filename){
        android.content.SharedPreferences prefs = context.getSharedPreferences(filename,Context.MODE_PRIVATE);
       String value = prefs.getString(key,"default");
       return value;

    }

    public static void setSharedPreferences(Context context,String filename, String key, String value){


        android.content.SharedPreferences.Editor editor = context.getSharedPreferences(filename,Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void deletePrefs(Context context, String filename){
        SharedPreferences.Editor editor = context.getSharedPreferences(filename,Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }
}
