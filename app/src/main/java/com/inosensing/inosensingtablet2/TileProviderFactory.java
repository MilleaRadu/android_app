package com.inosensing.inosensingtablet2;

import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class TileProviderFactory {


    public static GeoServerTileProvider getGeoServerTileProvider(){
        final String url_s = "inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=geojson_grid_bucuresti&outputFormat=application/json&srsname=EPSG:3857";


        GeoServerTileProvider tileProvider =
                new GeoServerTileProvider(256,256) {

                    @Override
                    public synchronized URL getTileUrl(int x, int y, int zoom) {
                        try {

//                            double[] bbox = getBoundingBox(x, y, zoom);
//
//                            String s = String.format(Locale.US, URL_STRING, bbox[MINX],
//                                    bbox[MINY], bbox[MAXX], bbox[MAXY]);
//
//                            Log.d("GeoServerTileURL", s);
//
                           URL url = null;

                            try {
                                url = new URL(url_s);
                            }
                            catch (MalformedURLException e) {
                                throw new AssertionError(e);
                            }

                            return url;
                        }
                        catch (RuntimeException e) {
                            Log.d("GeoServerTileException",
                                    "getTile x=" + x + ", y=" + y +
                                            ", zoomLevel=" + zoom +
                                            " raised an exception", e);
                            throw e;
                        }

                    }
                };
        return tileProvider;
    }

}
