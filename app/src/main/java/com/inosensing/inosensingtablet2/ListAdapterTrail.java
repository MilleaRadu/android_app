package com.inosensing.inosensingtablet2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.alvis.lazyload.ImageLoader;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ListAdapterTrail extends ArrayAdapter<Trip> {

    private Context context;
    int resource;
    ImageLoader imageLoader;

    public ListAdapterTrail(Context context, int adapter_view_trails, List<Trip> tripsList) {
        super(context, adapter_view_trails,tripsList);
        this.context = context;
        this.resource = adapter_view_trails;
        imageLoader = new ImageLoader(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            String time_started = getItem(position).getDate_started();
            String time_ended = getItem(position).getDate_ended();
            double speed = getItem(position).getAvgSpeed();
            double distance = getItem(position).getDistance();
            String trail_id = getItem(position).getTrail_id();
            List<LatLng> points = getItem(position).getPolylineBuffer();
            SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        try {
            Date date_started = sdf.parse(time_started);
            Date date_ended = sdf.parse(time_ended);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Trip trip = new Trip(points,speed,distance,time_started,time_ended,trail_id);

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource,parent,false);


            TextView tvTimeStarted = convertView.findViewById(R.id.Label_time_started);

        TextView tvTimeEnded = convertView.findViewById(R.id.Label_time_ended);
            TextView tvPosition  = convertView.findViewById(R.id.TextViewPosition);
        TextView tvDate  = convertView.findViewById(R.id.TextViewDate);
        TextView tvSpeed = convertView.findViewById(R.id.TextViewAvgSpeed);
        TextView tvDistance= convertView.findViewById(R.id.TextViewDistance);
        ImageView imageView = convertView.findViewById(R.id.ImageViewStaticMap);
        String encodedPath = PolyUtil.encode(trip.getPolylineBuffer());
        String url_string = "http://inosensing.go.ro:8080/images/worldmap.png";
        DecimalFormat df = new DecimalFormat("##.##");
        df.setRoundingMode(RoundingMode.DOWN);

        imageLoader.DisplayImage(url_string,imageView,android.R.drawable.presence_busy);
        tvTimeStarted.setText("Started at:"+time_started.split("T")[1].split("..00Z")[0]);
        tvTimeEnded.setText("Ended at:"+time_started.split("T")[1].split("..00Z")[0]);
        tvSpeed.setText(df.format(speed*3.6) + " km/h");
        tvDistance.setText(df.format(distance/1000) + " km");
        int index = position+1;
        tvDate.setText(time_started.split("T")[0]+" ") ;
        tvPosition.setText(index+".") ;



        return convertView;





    }
}



