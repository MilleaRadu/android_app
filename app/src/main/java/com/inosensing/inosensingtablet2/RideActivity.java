package com.inosensing.inosensingtablet2;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;



import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.UrlTileProvider;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.google.maps.android.SphericalUtil;



import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import javax.crypto.Mac;

import static android.view.View.GONE;

public class RideActivity extends AppCompatActivity {

    private static final int REQUEST_BL_PERMISSIONS = 3;
    private static final int REQUEST_BL_ADMIN_PERMISSIONS = 3;
    private BottomNavigationView navigation;
    private List<LatLng> polylineBuffer = new ArrayList<LatLng>();
   // private List<Double> speedBuffer = new ArrayList<Double>();
    private double distanceBuffer = 0;

    private static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_BT_PERMISSIONS = 0;
    public static final int REQUEST_LOCATION_PERMISSIONS = 2;
    private static final long SCAN_PERIOD = 5000;

    private AlertDialog dialog;
    private Button startSeviceButton;
    private Button stopServiceButton;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothLeScanner mBluetoothLeScanner = null;
    private TextView textMeasureDust;
    private TextView textMeasureTemp;
    private TextView textMeasureHum;
    private SupportMapFragment mapFragment;
    private GoogleMap map;

    private static int MEASURE_PERIOD = 0;
    public static final String EXTRA_MEASURE_PERIOD = "MEASURE_PERIOD";
    public static final String EXTRA_DEVICE_ADRESS = "DEVICE_ADRESS";
    private String date_started;
    private String date_ended;
    private Marker RiderMarker;




    private boolean mScanning = false;
    private Handler mHandler = null;

    private PolylineOptions polyline;
    private Polyline    polylineP;

    private List<String> DeviceNames = new ArrayList<String>();
    private List<BluetoothDevice> DeviceList = new ArrayList<BluetoothDevice>();
    private ArrayAdapter<String> listAdapter = null;
    private ListView listView;
    private String MAC;

    private ScanCallback mLeScanCallback =
            new ScanCallback() {

                @Override
                public void onScanResult(int callbackType, final ScanResult result) {


                    Toast.makeText(RideActivity.this, "found somth", Toast.LENGTH_SHORT).show();
                    //Log.d("=>>>>>>>>>>>>>>>>>>>>",result.getDevice().getAddress());
                    if(DeviceNames.contains(result.getDevice().getName())&&result.getDevice().getAddress().toLowerCase().trim().equals(MAC)){

                    }

                    if(!DeviceNames.contains(result.getDevice().getName())&&result.getDevice().getName()!=null){
                        DeviceList.add(result.getDevice());
                        DeviceNames.add(result.getDevice().getName());
                    }
                    listAdapter.notifyDataSetChanged();




                }

                @Override
                public void onScanFailed(int errorCode) {

                    Log.i("BLE", "onscanfailed");
                }
            };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride);


        polyline = new PolylineOptions();
        polyline.color(Color.BLUE);
        polyline.width(30);

        listAdapter = new ArrayAdapter<String>(RideActivity.this,android.R.layout.simple_list_item_1,DeviceNames);



        registerReceiver(mGattUpdateReceiver,makeGattUpdateIntentFilter());

        textMeasureDust = findViewById(R.id.textViewMeasureDust);
        textMeasureDust.setText(R.string.noMeasure);
        textMeasureTemp = findViewById(R.id.textViewMeasureTemp);
        textMeasureTemp.setText(R.string.noMeasure);
        textMeasureHum = findViewById(R.id.textViewMeasureHum);
        textMeasureHum.setText(R.string.noMeasure);

        navigation = findViewById(R.id.bottomnav);
        startSeviceButton = findViewById(R.id.buttonStartRide);

        stopServiceButton = findViewById(R.id.buttonStopRide);

        stopServiceButton.setVisibility(GONE);

        mHandler = new Handler();
        navigation.setSelectedItemId(R.id.nav_ride);

        if(ForegroundService.running){
            startSeviceButton.setVisibility(GONE);
            stopServiceButton.setVisibility(View.VISIBLE);
        }

        StartServiceListener();
        StopServiceListener();
        InitMap();
        NavListener();
        FetchPreferences();
    }


    void FetchPreferences(){
        MAC=Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_MAC,ConstantsAndKeys.FILE_NAME_PREFS);

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(ForegroundService.running){
            startSeviceButton.setVisibility(GONE);
            stopServiceButton.setVisibility(View.VISIBLE);
        }

        polyline=new PolylineOptions();
        polyline.color(Color.BLUE);
        polyline.width(30);
    }

    private void StartServiceListener() {

        startSeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSeviceButton.setVisibility(GONE);
                startSeviceButton.setEnabled(false);
                if(Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_USERNAME,ConstantsAndKeys.FILE_NAME_PREFS)!="default"){

                    mapFragment.getView().setVisibility(View.VISIBLE);
                    initBLE(v);


                }else{
                    Toast.makeText(RideActivity.this, "Please log in to use this service", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void StopServiceListener() {
        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    stopServiceButton.setEnabled(false);
                SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                UUID trail_id = ForegroundService.trail_id;
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                date_ended =sdf.format(Calendar.getInstance().getTime());
                if(Runner.mapPoints.size()>1){
                    AsyncTask sender = new AsyncTask<Trip,Void,String>() {

                        URL url;
                        @Override
                        protected String doInBackground(Trip[] objects) {

                            try{
                                String points = "";
                                for(int i =0 ; i < objects[0].getPolylineBuffer().size();i++){
                                    points=points.concat(objects[0].getPolylineBuffer().get(i).longitude+" "+objects[0].getPolylineBuffer().get(i).latitude+",");
                                }
                                Log.d("ForegroundService","POINTS STRING BEFORE SUBSTRING "+points);
                                points=points.substring(0,points.length()-1);

                                Log.d("ForegroundService","POINTS STRING  "+points);
                                Log.d("ForegroundService","poliline buffer size "+Runner.mapPoints.size());
                                Log.d("ForegroundService","poliline speed size "+Runner.speedList.size());
                                Log.d("ForegroundService","poliline distance "+distanceBuffer);
                                // post method with body written in serverside comments ,
                                // measure and send duration too....
                                JSONObject toSend = JsonFormatter(objects[0],points);
                                url = new URL("http://inosensing.go.ro:8080/trail");
                                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                                DataOutputStream printout;
                                DataInputStream input;
                                connection.setConnectTimeout(10*1000);
                                connection.setDoInput(true);
                                connection.setDoOutput(true);
                                connection.setUseCaches(false);
                                connection.setRequestProperty("Content-Type","application/json");
                                connection.setRequestMethod("POST");
                                connection.connect();
                                printout = new DataOutputStream(connection.getOutputStream());
                                printout.writeBytes(toSend.toString());
                                printout.flush();
                                printout.close();
                                int HttpResult = connection.getResponseCode();
                                if (HttpResult ==HttpURLConnection.HTTP_ACCEPTED){
                                    return "ACCEPTED";
                                }else{
                                    return "ERROR POSTING";
                                }


                            }catch(IOException e){
                                Log.d("ForegroundService", "Post trail: "+e.getLocalizedMessage());
                                return null;
                            }

                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            polylineBuffer.clear();
                            polyline = new PolylineOptions();
                            polyline.color(Color.BLUE);
                            polyline.width(30);
                            polylineP.remove();
                            //speedBuffer.clear();
                            distanceBuffer=0;

                            Toast.makeText(RideActivity.this, "TRAIL POST RESULT : "+s, Toast.LENGTH_SHORT).show();
                            Log.d("ForegroundService", "onPostExecute: send trail info  "+s);

                            Toast.makeText(RideActivity.this, Runner.mapPoints.toString(), Toast.LENGTH_SHORT).show();


                            mapFragment.getView().setVisibility(GONE);

                            stopService(v);
                            stopServiceButton.setVisibility(GONE);
                            startSeviceButton.setVisibility(View.VISIBLE);
                            startSeviceButton.setEnabled(true);

                        }
                    }.execute(new Trip(Runner.mapPoints,CalculateAvgSpeed(Runner.speedList),distanceBuffer,date_started,date_ended,trail_id.toString()));
                }else{
                    mapFragment.getView().setVisibility(GONE);

                    stopService(v);
                    stopServiceButton.setVisibility(GONE);
                    startSeviceButton.setVisibility(View.VISIBLE);
                    startSeviceButton.setEnabled(true);
                }



            }
        });
    }

    private double CalculateAvgSpeed(List<Double> speedBuffer) {
        double avg = 0;
        for(int i = 0 ; i < speedBuffer.size();i++){
            avg+=speedBuffer.get(i);
        }

        return avg/speedBuffer.size();
    }

    private void NavListener() {
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nav_overview:
                        Intent overview = new Intent(getApplicationContext(),OverviewActivity.class);
                        startActivity(overview);
                    case R.id.nav_ride :

                        return true;
                    case R.id.nav_history:
                        Intent history = new Intent(getApplicationContext(), HistoryActivity.class);

                        startActivity(history);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_help:
                        Intent help = new Intent(getApplicationContext(), HelpActivity.class);

                        startActivity(help);
                        overridePendingTransition(1,1);
                        return true;
                    case R.id.nav_home :
                        Intent home = new Intent(getApplicationContext(), MainActivity.class);

                        startActivity(home);
                        overridePendingTransition(1,1);
                        return true;
                }
                return false;
            }
        });
    }



    public void InitMap(){
        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.MapFragment);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;





                RiderMarker= map.addMarker(new MarkerOptions()
                        .visible(false)
                        .position(new LatLng(0,0))
                        .draggable(false)
                        .icon(RideActivity.generateBitmapDescriptorFromRes(RideActivity.this,R.drawable.ic_navigation)));
                    //mapFragment.getView().setVisibility(View.GONE);
                   polylineP= map.addPolyline(polyline);


            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver,makeGattUpdateIntentFilter());

    }


    public void initBLE(View v) {



        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(RideActivity.this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return;
        } else {
            if(mBluetoothAdapter==null){
                Toast.makeText(this, "adapter is null", Toast.LENGTH_SHORT).show();
                final BluetoothManager bluetoothManager =
                        (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
                mBluetoothAdapter = bluetoothManager.getAdapter();
                if (mBluetoothAdapter == null) {
                    Toast.makeText(this, R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    enableBt();
                }
            }else{
                enableBt();
                Toast.makeText(this, "adapter is not null, calling startService", Toast.LENGTH_SHORT).show();

            }

        }
    }



    public void startService(){


            Log.d("START SERVICE", "CALLED startService");


            if(mScanning){
                scanLeDevice(false);


            }else{

                scanLeDevice(true);

            }
        }










    private void ShowDialogListView() {
        Log.d("ForegroundService", "CALLED");
        AlertDialog.Builder builder = new AlertDialog.Builder(RideActivity.this);

        View v = getLayoutInflater().inflate(R.layout.scan_dialog_view,null);
        listView = v.findViewById(R.id.deviceListView);
        ListViewListener();
        listView.setAdapter(listAdapter);
        builder.setCancelable(true);
        builder.setMessage("Choose your desired BLE device");
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeviceNames.clear();
                scanLeDevice(false);
                dialog.dismiss();
                startSeviceButton.setEnabled(true);
            }
        });
        builder.setView(v);

         dialog = builder.create();
        dialog.show();
    }

     public void startServiceWithMAC(String MAC){
         Intent serviceIntent = new Intent(RideActivity.this, ForegroundService.class);
         serviceIntent.putExtra(EXTRA_MEASURE_PERIOD,MEASURE_PERIOD);
         serviceIntent.putExtra(EXTRA_DEVICE_ADRESS,MAC);
     }

    private void ListViewListener() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String isMACSet=Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_MAC,ConstantsAndKeys.FILE_NAME_PREFS);
                if(isMACSet.trim().toLowerCase().equals("default")&&DeviceList.get(position).getName().trim().toLowerCase().equals("bt05")){

                        Log.d("RideActivity", "CALLED MAC ADRESS Q");
                        AlertDialog.Builder builder = new AlertDialog.Builder(RideActivity.this);

                        View v = getLayoutInflater().inflate(R.layout.mac_dialog,null);
                       TextView TviewQ = v.findViewById(R.id.TextViewMAc);
                       TviewQ.setText("Save this one as your own?");
                        builder.setCancelable(true);
                        builder.setMessage("MAC ADRESS : " + DeviceList.get(position).getAddress());
                        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                                dialog.dismiss();
                            }
                        });
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_MAC,ConstantsAndKeys.FILE_NAME_PREFS,DeviceList.get(position).getAddress().toLowerCase().trim());
                            }
                        });
                        builder.setView(v);

                        dialog = builder.create();
                        dialog.show();
                    }else{
                    Toast.makeText(RideActivity.this, "Mac is not default", Toast.LENGTH_SHORT).show();

                }

                final BluetoothDevice chosenDevice = DeviceList.get(position);
                if(!chosenDevice.getName().toLowerCase().trim().equals("bt05")){
                    Toast.makeText(RideActivity.this, "Chosen device is not the expected, please retry connection", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    return;
                }
                if(!(chosenDevice.getName().equals(DeviceNames.get(position)))){
                    Toast.makeText(RideActivity.this, "chosen device not good", Toast.LENGTH_SHORT).show();
                    return ;
                }else{

                    SendServiceIntent(chosenDevice);

                    Toast.makeText(RideActivity.this, "Service intent sent", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }

            }
        });
    }

    private void SendServiceIntent(BluetoothDevice chosenDevice) {
        Log.d("ForegroundService", chosenDevice.getName());

        String period="5";

        Intent serviceIntent = new Intent(RideActivity.this, ForegroundService.class);
        serviceIntent.putExtra(EXTRA_MEASURE_PERIOD,MEASURE_PERIOD);
        serviceIntent.putExtra(EXTRA_DEVICE_ADRESS,chosenDevice.getAddress());


        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        startSeviceButton.setVisibility(GONE);
        stopServiceButton.setVisibility(View.VISIBLE);
        stopServiceButton.setEnabled(true);
        startService(serviceIntent);
    }


    public void scanLeDevice(final boolean enable) {

        if(mBluetoothAdapter.getBluetoothLeScanner()!=null){
            mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }else{
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
            mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }


        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothLeScanner.stopScan(mLeScanCallback);
                    ShowDialogListView();

                }
            },SCAN_PERIOD);
                    mScanning = true;
                    mBluetoothLeScanner.startScan(mLeScanCallback);


        } else {

            mScanning = false;
            mBluetoothLeScanner.stopScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

    private void checkBtPermissions() {
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION_PERMISSIONS);
        }else{
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),Manifest.permission.BLUETOOTH)!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH},REQUEST_BL_PERMISSIONS);

            }else{
                if(ContextCompat.checkSelfPermission(this.getApplicationContext(),Manifest.permission.BLUETOOTH_ADMIN)!=PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH_ADMIN},REQUEST_BL_ADMIN_PERMISSIONS);
                }else{
                    startService();
                }

            }

        }
    }


    public void enableBt(){

        if (mBluetoothAdapter == null||!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent,REQUEST_ENABLE_BT);
        }else{
            checkBtPermissions();
        }
    }

    public void stopService(View v){
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.disable();
            mBluetoothAdapter=null;
        }

        DeviceList.clear();
        DeviceNames.clear();
        mBluetoothLeScanner=null;
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        stopService(serviceIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            checkBtPermissions();

        }else{
            Toast.makeText(this, "BLE could not be enabled", Toast.LENGTH_SHORT).show();

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch(requestCode){
            case REQUEST_BL_ADMIN_PERMISSIONS:{
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    checkBtPermissions();
                }

            }
            case REQUEST_BT_PERMISSIONS :{
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    checkBtPermissions();
                }

                }
            case REQUEST_LOCATION_PERMISSIONS :{
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    checkBtPermissions();
                }else{
                    finish();
                }



            }
        }

    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("ForegroundService" , "recieved somth" + intent.getAction());
            final String action = intent.getAction();
            if (Runner.DISPLAY_DATA.equals(action)){
                String dataIno = intent.getStringExtra(Runner.DATA_FROM_INO);
                String[] dataSplit = dataIno.split(" ");
                double lat = intent.getDoubleExtra(Runner.DATA_LATITUDE,0);
                double lon = intent.getDoubleExtra(Runner.DATA_LONGITUDE,0);
                textMeasureDust.setText(dataSplit[1]+"ug/m3 PM");
                textMeasureTemp.setText(dataSplit[0]+"Celsius Temp");
                textMeasureHum.setText(dataSplit[2]+"% Relative Humidity");


            }else{
                if(Runner.NEW_LOCATION_AND_SPEED.equals(action)){

                    //double speed = intent.getDoubleExtra(Runner.DATA_SPEED,0);
                    //speedBuffer.add(speed);
                    SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

                    date_started =sdf.format(Calendar.getInstance().getTime());

                    Toast.makeText(RideActivity.this, "recieved location and speed data", Toast.LENGTH_SHORT).show();


                       // polylineBuffer.add(new LatLng(lat,lon));

                        if(Runner.mapPoints.size()>1){
                            AddDistanceToBuffer(Runner.mapPoints.get(Runner.mapPoints.size()-1).latitude,Runner.mapPoints.get(Runner.mapPoints.size()-1).longitude);
                        }

                        polylineP.remove();

                        polyline = null;


                        polylineP=map.addPolyline( new PolylineOptions().width(30).color(Color.BLUE).addAll(Runner.mapPoints));
                        RiderMarker.setVisible(true);
                        RiderMarker.setPosition(Runner.mapPoints.get(Runner.mapPoints.size()-1));


                            if(Runner.mapPoints.size()>1){
                                double rotation = 180+SphericalUtil.computeHeading(Runner.mapPoints.get(Runner.mapPoints.size()-1),Runner.mapPoints.get(Runner.mapPoints.size()-2));
                                map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(Runner.mapPoints.get(Runner.mapPoints.size()-1),18f,90f,(float) rotation)));

                            }else{

                                map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(Runner.mapPoints.get(Runner.mapPoints.size()-1),18f,90f,0)));
                            }


                }else{
                    if(Runner.DEVICE_DISCONNECTED.equals(action)){
                        Toast.makeText(RideActivity.this, "DEVICE DISCONNTECTED!!!!", Toast.LENGTH_LONG).show();
                    }
                }
            }

        }
    };

    private void AddDistanceToBuffer(double lat, double lon) {
        try{
            Location nou = new Location(LocationManager.GPS_PROVIDER);
            nou.setLatitude(lat);
            nou.setLongitude(lon);
            LatLng lastInLine = polylineP.getPoints().get(polylineP.getPoints().size()-1);
            Location last = new Location(LocationManager.GPS_PROVIDER);
            last.setLatitude(lastInLine.latitude);
            last.setLongitude(lastInLine.longitude);
            distanceBuffer+=last.distanceTo(nou);
            Toast.makeText(this, "Distanta adaugata: " +last.distanceTo(nou)+"m/s ? ", Toast.LENGTH_SHORT).show();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Runner.ACTION_GATT_CONNECTED);
        intentFilter.addAction(Runner.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(Runner.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(Runner.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(Runner.DISPLAY_DATA);
        intentFilter.addAction(Runner.NEW_LOCATION_AND_SPEED);

        return intentFilter;
    }

    private JSONObject JsonFormatter(Trip trip, String points) {

        try {
            final JSONObject obj = new JSONObject();

            obj.put("user_id",Preferences.getSharedPreferences(getApplicationContext(),ConstantsAndKeys.KEY_PREF_ID,ConstantsAndKeys.FILE_NAME_PREFS));
            obj.put("numberOfPoints",trip.getPolylineBuffer().size());
            obj.put("points",points);
            obj.put("average_speed", trip.getAvgSpeed());
            obj.put("time_started", trip.getDate_started());
            obj.put("time_ended", trip.getDate_ended());
            obj.put("distance", trip.getDistance());
            obj.put("trail_id",trip.getTrail_id());




            return obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();

    }

    public static BitmapDescriptor generateBitmapDescriptorFromRes(
            Context context, int resId) {
        Drawable drawable = ContextCompat.getDrawable(context, resId);
        drawable.setBounds(
                0,
                0,
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


}
