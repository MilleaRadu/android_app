package com.inosensing.inosensingtablet2;

import java.util.UUID;

public class User {

    private UUID ID;
    private String surname;
    private String forname;
    private String email;
    private String password;
    private String username;
    private String MAC;

    public User(UUID ID, String surname, String forname, String email, String password, String username) {
        this.ID = ID;
        this.surname = surname;
        this.forname = forname;
        this.email = email;
        this.password = password;
        this.username = username;
    }

    public User(UUID ID, String surname, String forname, String email, String password, String username, String MAC) {
        this.ID = ID;
        this.surname = surname;
        this.forname = forname;
        this.email = email;
        this.password = password;
        this.username = username;
        this.MAC = MAC;
    }

    public User(){

    }

    public UUID getID() {
        return ID;
    }

    public void setID(UUID ID) {
        this.ID = ID;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getForname() {
        return forname;
    }

    public void setForname(String forname) {
        this.forname = forname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }
}
