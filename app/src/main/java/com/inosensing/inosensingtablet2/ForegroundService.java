package com.inosensing.inosensingtablet2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.inosensing.inosensingtablet2.R;

import java.util.UUID;

public class ForegroundService extends Service {

    private static final String TAG = "ForegroundService";
    public static final String NOTIFICATION_CHANNEL_ID = "MyServiceChannel";

    private IBinder mBinder = new MyBinder();
    private Handler mHandler;
    private Runner runner;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    protected BluetoothGatt mBluetoothGatt;

    private Thread thread;

    private int mConnectionState = STATE_DISCONNECTED;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    private int period;
    private String deviceAdress;
    public static boolean running= false;
    public static UUID trail_id;

    @Override
    public void onCreate() {
        super.onCreate();
        trail_id = UUID.randomUUID();
        Log.d(TAG, "onCreate: trail_id "+trail_id);
        //init objects;
    }
    private void createNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel serviceChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    "Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!running){
            running=true;
        }
        period = intent.getIntExtra(RideActivity.EXTRA_MEASURE_PERIOD,5);

        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        deviceAdress =intent.getStringExtra(RideActivity.EXTRA_DEVICE_ADRESS);
        createNotificationChannel();
        Intent notificationIntent = new Intent (this, RideActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);


        Notification notification = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID )
                .setContentTitle("InoSensing Ride")
                .setContentText("InoSensing Measuring Service is still running (click here to terminate the service) ")
                .setSmallIcon(R.drawable.ic_navigation)
                .setContentIntent(pendingIntent)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .build();

        startForeground(1,notification);
        runner = new Runner(period,deviceAdress,getApplicationContext(),trail_id.toString());
         thread = new Thread(runner);

        Log.d(TAG,"Starting thread");
        thread.start();



        ///work;


        //stopSelf();

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {//only needed for bind service for comm with other components
        return null;
    }

    public class MyBinder extends Binder {
        ForegroundService getService() {
            return ForegroundService.this;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {

        //close();
        return super.onUnbind(intent);
    }

    public void startMeasurements(Notification notification, int period){



    }

    @Override
    public void onDestroy() {

        thread.interrupt();
        runner.close();
        thread=null;
        running=false;
        trail_id = null;
//        runner.disconnect();
        Log.d(TAG, "onDestroy: destroyed");

        super.onDestroy();
    }
}
