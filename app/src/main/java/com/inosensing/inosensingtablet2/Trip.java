package com.inosensing.inosensingtablet2;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Trip implements Serializable {

    private List<LatLng> polylineBuffer;
    private double avgSpeed;
    private double distance;
    private String date_started;
    private String date_ended;
    private String trail_id;


    public  Trip(List<LatLng>  polylineBuffer, double avgSpeed , double distance, String date_started , String date_ended, String trail_id){
        this.polylineBuffer = new ArrayList<LatLng>();
        this.polylineBuffer = polylineBuffer;
        this.avgSpeed = avgSpeed;
        this.distance=distance;
        this.date_ended = date_ended;
        this.date_started = date_started;
        this.trail_id = trail_id;



    }

    public Trip(double avgSpeed, double distance, String date_started, String date_ended, String trail_id) {
        this.avgSpeed = avgSpeed;
        this.distance = distance;
        this.date_started = date_started;
        this.date_ended = date_ended;
        this.trail_id = trail_id;
    }

    public  List<LatLng> getPolylineBuffer() {
        return polylineBuffer;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public double getDistance() {
        return distance;
    }

    public String getTrail_id() {
        return trail_id;
    }

    public void setTrail_id(String trail_id) {
        this.trail_id = trail_id;
    }

    public String getDate_started() {
        return date_started;
    }

    public String getDate_ended() {
        return date_ended;
    }
}
