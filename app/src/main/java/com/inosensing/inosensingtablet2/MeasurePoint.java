package com.inosensing.inosensingtablet2;

import com.google.android.gms.maps.model.LatLng;

public class MeasurePoint {

    private double dust;
    private double humidity;
    private double temperature;
    private LatLng coords;
    private String datetime;
    public static double maxValue= 0;
    public static double minValue = 500;

    public MeasurePoint(double dust, double humidity, double temperature, LatLng coords,String datetime) {
        this.dust = dust;
        this.humidity = humidity;
        this.temperature = temperature;
        this.coords = coords;
        this.datetime = datetime;
        if(dust>maxValue){
            maxValue=Math.round(dust);
        }
        if(dust<minValue){
            minValue=Math.round(dust);

        }

    }


    public double getDust() {
        return dust;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDust(double dust) {
        this.dust = dust;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "MeasurePoint{" +
                "dust=" + dust +
                ", humidity=" + humidity +
                ", temperature=" + temperature +
                ", coords=" + coords +
                ", datetime='" + datetime + '\'' +
                '}';
    }

    public LatLng getCoords() {
        return coords;
    }

    public void setCoords(LatLng coords) {
        this.coords = coords;
    }

    public static void ResetInterval(){
        maxValue = 0;
        minValue = 500;
    }
}
