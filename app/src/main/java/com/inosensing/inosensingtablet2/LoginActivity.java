package com.inosensing.inosensingtablet2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.editTextEmailUserName);
        password = findViewById(R.id.editPasswordLogin);
        login = findViewById(R.id.buttonLogin);
        ListenerLogin();
    }

    private void ListenerLogin() {
        //check credentials
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new  AsyncTask<String,Void,JSONArray>(){
                    URL url;
                    @Override
                    public JSONArray doInBackground(String... voids) {
                        try {
                            url = new URL("http://inosensing.go.ro:8080/user"+"/"+voids[0]+"/"+voids[1]);
                            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                            connection.setDoInput(true);
                            connection.setUseCaches(false);
                            connection.setRequestProperty("Accept-Encoding","application/json");
                            connection.setRequestProperty("Content-Type","application/json");
                            connection.setRequestMethod("GET");
                            connection.connect();
                            if(connection.getResponseCode()==200){

//
//                                String stringResult = String.valueOf(connection.getContent());
//                                Log.v("AsuncTask", stringResult);
                                BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                StringBuilder total = new StringBuilder();
                                for (String line; (line = r.readLine()) != null; ) {
                                    total.append(line).append('\n');
                                }
                                JSONArray result = new JSONArray(total.toString());
                                Log.v("LoginActivity", result.toString());
                                return result;
                            }else{
                                Log.v("LoginActivity",url.toString());
                                Log.v("LoginActivity", String.valueOf(connection.getResponseCode()));
                            }


                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @SuppressLint("StaticFieldLeak")
                    @Override
                    protected void onPostExecute(JSONArray jsonArray) {
                        super.onPostExecute(jsonArray);
                        if (jsonArray == null) {
                            Toast.makeText(LoginActivity.this, "result is null", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        try {
                            if(jsonArray.getJSONObject(0)!=null){
                                try {
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_USERNAME,jsonArray.getJSONObject(0).getString("username"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_PASSWORD,jsonArray.getJSONObject(0).getString("password"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_SURNAME,jsonArray.getJSONObject(0).getString("surname"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_FORNAME,jsonArray.getJSONObject(0).getString("forname"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_EMAIL,jsonArray.getJSONObject(0).getString("email"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_MAC,jsonArray.getJSONObject(0).getString("MAC"));
                                    Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_ID,jsonArray.getJSONObject(0).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.execute(username.getText().toString().trim().toLowerCase(),password.getText().toString().trim().toLowerCase());


            }
        });

    }



    @Override
    public void onBackPressed() {


        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
    }


}

class CredentialChecker extends AsyncTask<String,Void, JSONArray>{

    URL url;
    @Override
    protected JSONArray doInBackground(String... voids) {
        try {
            url = new URL("http://inosensing.go.ro:8080/user"+voids[0]+"/"+voids[1]);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            DataInputStream input;
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestMethod("GET");
            connection.connect();
            String stringResult = connection.getResponseMessage();
            JSONArray result = new JSONArray(stringResult);
            return result;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        super.onPostExecute(jsonArray);
//        if(jsonArray.length()>0){
//            Preferences.setSharedPreferences(getApplicationContext(),ConstantsAndKeys.FILE_NAME_PREFS,ConstantsAndKeys.KEY_PREF_USERNAME,username.getText().toString().trim().toLowerCase());
//            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
//            startActivity(intent);
     //   }
    }
}
